<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'Name',
        'Street',
        'Village',
        'County',
        'PostCode',
        'Email'
    ];
}
