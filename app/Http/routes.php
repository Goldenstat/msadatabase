<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect()->route('customer.index');
});

get('/cards', function () {
    $customers = \App\Customer::all();

    return view('customer.cards', compact('customers'));
});

get('customer/print/{data}', ['as' => 'customer.print', 'uses' => 'CustomerController@renderPdf']);
resource('customer', 'CustomerController');
