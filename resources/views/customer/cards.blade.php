@extends('master')

@section('content')

    @foreach($customers->chunk(3) as $chunk)

        <div class="card-deck">
            @foreach($chunk as $customer)
            <div class="card">
                <img class="card-img-top" src="http://placehold.it/350x150" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">{{$customer->Name}}e</h4>
                    <p class="card-text">Adress: {{$customer->Street}}, {{$customer->Village}}, {{$customer->County}}</p>
                    <p class="card-text">Post Code: {{$customer->PostCode}}</p>
                    <p class="card-text">Email: {{$customer->Email}}</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            @endforeach
        </div>
    @endforeach

@stop