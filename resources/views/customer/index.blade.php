@extends('master')

@section('content')

    <div class="table-responsive table-hover">
        <table class="table table-striped">
            <thead class="thead-inverse">
                <tr>
                    <th><a href="#" id="selectAll"><i class="fa fa-plus-circle"></i></a></th>
                    <th>#</th>
                    <th>Name</th>
                    <th>Street</th>
                    <th>Village</th>
                    <th>County</th>
                    <th>Post Code</th>
                    <th>Email</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td><input type="checkbox" name="print" value="{{$customer->id}}"></td>
                        <td>{{$customer->id}}</td>
                        <td>{{$customer->Name}}</td>
                        <td>{{$customer->Street}}</td>
                        <td>{{$customer->Village}}</td>
                        <td>{{$customer->County}}</td>
                        <td>{{$customer->PostCode}}</td>
                        <td><a class="btn btn-info" href="mailto:{{$customer->Email}}"><i class="fa fa-envelope-o fa-md"></i> Send email</td>
                        <td>
                            <form action="{{route('customer.destroy', $customer->id)}}" method="POST">
                                {!! csrf_field() !!}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash-o fa-md"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#selectAll').click(function() {
                $('input[name=print]').attr('checked', true);
            });
        });
        $('#Print').click(function() {
            var data = {};
            var dataStrings = [];

            $('input[type="checkbox"]').each(function() {
                if (this.checked) {
                    if (data[this.name] === undefined) data[this.name] = [];
                    data[this.name].push(this.value);
                }
            });

            $.each(data, function(key, value)
            {
                dataStrings.push(value.join('_'));
            });

            var result = dataStrings.join('&');

            window.location.href = "customer/print/" + result;
        });

    </script>
@stop