<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Michael Spriggs Agencies Customer Database</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <nav class="navbar navbar-dark bg-primary">
        <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
            &#9776;
        </button>
        <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
            <a class="navbar-brand" href="#">Michael Spriggs Agencies</a>
            <ul class="nav navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">
                        <i class="fa fa-user fa-lg"></i>
                        Customers <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#modal-customer-create">
                        <i class="fa fa-user-plus fa-lg"></i>
                        Add Customer
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">With Selected</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" id="Print"><i class="fa fa-print"></i> Print</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#"><i class="fa fa-envelope"></i> Send email</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        @include('partials.messages')
        @yield('content')
    </div>

    @include('partials.modals')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/all.min.js"></script>

    @yield('scripts')


</body>
</html>