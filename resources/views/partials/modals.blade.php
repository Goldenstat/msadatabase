{{-- Add Customer Modal --}}
<div class="modal fade" id="modal-customer-create">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{route('customer.store')}}"
                  class="form-horizontal">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Add new customer</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="Name" class="col-sm-3 control-label">
                            Customer Name
                        </label>
                        <div class="sol-sm-8">
                            <input type="text" id="Name" name="Name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Street" class="col-sm-3 control-label">
                            Customer Street
                        </label>
                        <div class="sol-sm-8">
                            <input type="text" id="Street" name="Street" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Village" class="col-sm-3 control-label">
                            Customer Village
                        </label>
                        <div class="sol-sm-8">
                            <input type="text" id="Village" name="Village" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="County" class="col-sm-3 control-label">
                            Customer County
                        </label>
                        <div class="sol-sm-8">
                            <input type="text" id="County" name="County" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Postcode" class="col-sm-3 control-label">
                            Customer Post Code
                        </label>
                        <div class="sol-sm-8">
                            <input type="text" id="Postcode" name="PostCode" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Email" class="col-sm-3 control-label">
                            Customer Email
                        </label>
                        <div class="sol-sm-8">
                            <input type="text" id="Email" name="Email" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add Customer</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{-- End Add Customer Modal --}}