<style>
    tr {
        padding-bottom: 2em;
    }
</style>
<div class="table-responsive table-hover">
    <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
            <th>Name</th>
            <th>Street</th>
            <th>Village</th>
            <th>County</th>
            <th>Post Code</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->Name}}</td>
                <td>{{$customer->Street}}</td>
                <td>{{$customer->Village}}</td>
                <td>{{$customer->County}}</td>
                <td>{{$customer->PostCode}}</td>
                <td>{{$customer->Email}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
